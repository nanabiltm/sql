Microsoft Windows [Version 10.0.18363.1440]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\Users\NABIL>cd ..

C:\Users>cd ..

C:\>cd xampp/mysql/bin

C:\xampp\mysql\bin>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.21-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
5 rows in set (0.049 sec)

----------------------------------SOAL NOMOR 1 MEMBUAT DATABASE---------------------------------

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [(none)]> use myshop;
Database changed

--------------------------SOAL NOMOR 2 MEMBUAT TABEL DI DALAM DATABASE--------------------------

MariaDB [myshop]> create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Query OK, 0 rows affected (0.213 sec)

MariaDB [myshop]> create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );
Query OK, 0 rows affected (0.215 sec)

MariaDB [myshop]> create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar (255),
    -> price int(9),
    -> stock int(9),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );
Query OK, 0 rows affected (0.236 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(8)       | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.007 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(8)       | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.007 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(8)       | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(9)       | YES  |     | NULL    |                |
| stock       | int(9)       | YES  |     | NULL    |                |
| category_id | int(8)       | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.007 sec)

-----------------------------SOAL NOMOR 3 MEMASUKKAN DATA PADA TABEL----------------------------

MariaDB [myshop]> insert into users(name, email, password)
    -> values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123")
    -> ;
Query OK, 2 rows affected (0.107 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

MariaDB [myshop]> insert into categories(name)
    -> values("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded")
    -> ;
Query OK, 5 rows affected (0.095 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,category_id)
    -> values("Sumsang","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1)
    -> ;
Query OK, 3 rows affected (0.101 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from items;
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | description                       | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang    | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh   | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

------------------------------SOAL NOMOR 4 MENGAMBIL DATA DARI DATABASE--------------------------

MariaDB [myshop]> select id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where price>1000000;
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | description                       | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang    | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where name like 'sum%';
+----+---------+-------------------------------+---------+-------+-------------+
| id | name    | description                   | price   | stock | category_id |
+----+---------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+---------+-------------------------------+---------+-------+-------------+
1 row in set (0.001 sec)

MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
+------------+-----------------------------------+---------+-------+-------------+--------+
| name       | description                       | price   | stock | category_id | name   |
+------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang    | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget |
| Uniklooh   | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.001 sec)

---------------------------SOAL NOMOR 5 MENGUBAH DATA DARI DATABASE------------------------------

MariaDB [myshop]> update items set name='Sumsang b50', price=2500000 where id=1;
Query OK, 1 row affected (0.069 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)